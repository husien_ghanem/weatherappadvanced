import 'package:flutter/material.dart';

Widget getImageAndData(
    {required String image,
    required String data,
    required String nameOfData,
    required double scal}) {
  return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        Image.asset(
          image,
          scale: scal,
        ),
        Container(alignment: Alignment.center,
    
            width: 77,
            child: Text(
              data,
              maxLines: 1,
              overflow: TextOverflow.ellipsis,
            )),
        const SizedBox(
          height: 6,
        ),
        Text(nameOfData),
      ]);
}
