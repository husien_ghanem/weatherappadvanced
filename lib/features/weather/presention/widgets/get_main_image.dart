import 'package:flutter/material.dart';

Widget getImagesLocalAccordingApi({required String imageString}) {
  final valueOfWeather = imageString;
 
  switch (valueOfWeather) {
    case '//cdn.weatherapi.com/weather/64x64/day/113.png':
      return Image.asset("assets/images/weather_icons/35.png");
    case '//cdn.weatherapi.com/weather/64x64/day/116.png':
      return Image.asset("assets/images/weather_icons/42.png");
    case '//cdn.weatherapi.com/weather/64x64/day/119.png':
    case '//cdn.weatherapi.com/weather/64x64/day/122.png':
      return Image.asset("assets/images/weather_icons/43.png");
    case '//cdn.weatherapi.com/weather/64x64/day/143.png':
    case '//cdn.weatherapi.com/weather/64x64/day/185.png':
      return Image.asset("assets/images/weather_icons/7.png");
    case '//cdn.weatherapi.com/weather/64x64/day/176.png':
    case '//cdn.weatherapi.com/weather/64x64/day/182.png':
    case '//cdn.weatherapi.com/weather/64x64/day/179.png':
    case '//cdn.weatherapi.com/weather/64x64/day/227.png':
    case '//cdn.weatherapi.com/weather/64x64/day/230.png':
    case '//cdn.weatherapi.com/weather/64x64/day/248.png':
    case '//cdn.weatherapi.com/weather/64x64/day/260.png':
    case '//cdn.weatherapi.com/weather/64x64/day/263.png':
    case '//cdn.weatherapi.com/weather/64x64/day/266.png':
    case '//cdn.weatherapi.com/weather/64x64/day/281.png':
    case '//cdn.weatherapi.com/weather/64x64/day/284.png':
    case '//cdn.weatherapi.com/weather/64x64/day/293.png':
    case '//cdn.weatherapi.com/weather/64x64/day/296.png':
    case '//cdn.weatherapi.com/weather/64x64/day/299.png':
    case '//cdn.weatherapi.com/weather/64x64/day/302.png':
    case '//cdn.weatherapi.com/weather/64x64/day/305.png':
    case '//cdn.weatherapi.com/weather/64x64/day/308.png':
    case '//cdn.weatherapi.com/weather/64x64/day/311.png':
      return Image.asset("assets/images/weather_icons/8.png");
    case '//cdn.weatherapi.com/weather/64x64/day/314.png':
    case '//cdn.weatherapi.com/weather/64x64/day/329.png':
    case '//cdn.weatherapi.com/weather/64x64/day/317.png':
    case '//cdn.weatherapi.com/weather/64x64/day/320.png':
    case '//cdn.weatherapi.com/weather/64x64/day/323.png':
    case '//cdn.weatherapi.com/weather/64x64/day/326.png':
    case '//cdn.weatherapi.com/weather/64x64/day/332.png':
    case '//cdn.weatherapi.com/weather/64x64/day/335.png':
    case '//cdn.weatherapi.com/weather/64x64/day/338.png':
    case '//cdn.weatherapi.com/weather/64x64/day/350.png':
    case '//cdn.weatherapi.com/weather/64x64/day/365.png':
    case '//cdn.weatherapi.com/weather/64x64/day/362.png':
    case '//cdn.weatherapi.com/weather/64x64/day/359.png':
    case '//cdn.weatherapi.com/weather/64x64/day/356.png':
    case '//cdn.weatherapi.com/weather/64x64/day/353.png':
      return Image.asset("assets/images/weather_icons/22.png");
    case '//cdn.weatherapi.com/weather/64x64/day/200.png':
    case '//cdn.weatherapi.com/weather/64x64/day/386.png':
    case '//cdn.weatherapi.com/weather/64x64/day/395.png':
    case '//cdn.weatherapi.com/weather/64x64/day/392.png':
    case '//cdn.weatherapi.com/weather/64x64/day/388.png':
    case '//cdn.weatherapi.com/weather/64x64/day/389.png':
      return Image.asset("assets/images/weather_icons/16.png");
    case '//cdn.weatherapi.com/weather/64x64/night/176.png':
    case '//cdn.weatherapi.com/weather/64x64/night/182.png':
    case '//cdn.weatherapi.com/weather/64x64/night/185.png':
    case '//cdn.weatherapi.com/weather/64x64/night/263.png':
    case '//cdn.weatherapi.com/weather/64x64/night/296.png':

    case '//cdn.weatherapi.com/weather/64x64/night/293.png':
    case '//cdn.weatherapi.com/weather/64x64/night/284.png':
    case '//cdn.weatherapi.com/weather/64x64/night/281.png':
    case '//cdn.weatherapi.com/weather/64x64/night/266.png':

    case '//cdn.weatherapi.com/weather/64x64/night/308.png':
    case '//cdn.weatherapi.com/weather/64x64/night/305.png':
    case '//cdn.weatherapi.com/weather/64x64/night/302.png':
    case '//cdn.weatherapi.com/weather/64x64/night/299.png':
    case '//cdn.weatherapi.com/weather/64x64/night/359.png':
    case '//cdn.weatherapi.com/weather/64x64/night/356.png':
    case '//cdn.weatherapi.com/weather/64x64/night/353.png':
      return Image.asset("assets/images/weather_icons/1.png");
    case '//cdn.weatherapi.com/weather/64x64/night/179.png':
    case '//cdn.weatherapi.com/weather/64x64/night/260.png':
    case '//cdn.weatherapi.com/weather/64x64/night/248.png':
    case '//cdn.weatherapi.com/weather/64x64/night/227.png':
    case '//cdn.weatherapi.com/weather/64x64/night/230.png':
    case '//cdn.weatherapi.com/weather/64x64/night/326.png':
    case '//cdn.weatherapi.com/weather/64x64/night/323.png':
    case '//cdn.weatherapi.com/weather/64x64/night/320.png':
    case '//cdn.weatherapi.com/weather/64x64/night/317.png':
    case '//cdn.weatherapi.com/weather/64x64/night/314.png':
    case '//cdn.weatherapi.com/weather/64x64/night/350.png':
    case '//cdn.weatherapi.com/weather/64x64/night/338.png':
    case '//cdn.weatherapi.com/weather/64x64/night/335.png':

    case '//cdn.weatherapi.com/weather/64x64/night/332.png':
    case '//cdn.weatherapi.com/weather/64x64/night/329.png':
    case '//cdn.weatherapi.com/weather/64x64/night/365.png':
    case '//cdn.weatherapi.com/weather/64x64/night/362.png':
    case '//cdn.weatherapi.com/weather/64x64/night/377.png':
    case '//cdn.weatherapi.com/weather/64x64/night/374.png':
    case '//cdn.weatherapi.com/weather/64x64/night/371.png':
    case '//cdn.weatherapi.com/weather/64x64/night/311.png':

    case '//cdn.weatherapi.com/weather/64x64/night/368.png':
      return Image.asset("assets/images/weather_icons/2.png");
    case '//cdn.weatherapi.com/weather/64x64/night/143.png':
      return Image.asset("assets/images/weather_icons/15.png");
    case '//cdn.weatherapi.com/weather/64x64/night/113.png':
      return Image.asset("assets/images/weather_icons/10.png");
    case '//cdn.weatherapi.com/weather/64x64/night/200.png':
    case '//cdn.weatherapi.com/weather/64x64/night/386.png':
    case '//cdn.weatherapi.com/weather/64x64/night/395.png':
    case '//cdn.weatherapi.com/weather/64x64/night/392.png':
    case '//cdn.weatherapi.com/weather/64x64/night/389.png':
      return Image.asset("assets/images/weather_icons/11.png");
    case '//cdn.weatherapi.com/weather/64x64/night/116.png':
    case '//cdn.weatherapi.com/weather/64x64/night/119.png':
      return Image.asset("assets/images/weather_icons/15.png");
    case '//cdn.weatherapi.com/weather/64x64/night/122.png':
      return Image.asset("assets/images/weather_icons/43.png");

    default:
      return Image.asset("assets/images/weather_icons/33.png");
  }
}
