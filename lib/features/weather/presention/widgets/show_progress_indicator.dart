import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:weather_app/core/resources/color_manager.dart';


class LoadingWidget extends StatelessWidget {
  final double? size;
  const LoadingWidget({super.key, this.size});

  @override
  Widget build(BuildContext context) {
    return Container(color: ColorManager.white,
      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Center(
            child: SpinKitSpinningCircle(
          color: Colors.blue,
          size: size ?? 120,
        )),
      ),
    );
  }
}
