// ignore_for_file: use_build_context_synchronously

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:weather_app/core/functions.dart';
import 'package:weather_app/features/weather/presention/bloc/weather_bloc.dart';
import 'package:weather_app/features/weather/presention/pages/city_weather.dart';
import 'package:weather_app/features/weather/presention/pages/search_page.dart';

import '../../../../core/resources/constans.dart';
import '../../domain/usecases/get_weather_forecast_usecase.dart';
import '../widgets/show_progress_indicator.dart';

class SplashScreen extends StatefulWidget {
  const SplashScreen({super.key});

  @override
  State<SplashScreen> createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  
  void getNextScreen() async {
    final bool isSelected = await isDefaultLocationSelected();
    await Future.delayed(const Duration(seconds: 2));
    if (isSelected) {
      final String defaultCityName = await getDefaultLocationSelected();
      // context.read<WeatherBloc>().add(
      //     GetWeatherCityEvent(GetWeatherForeCastParams(city: defaultCityName)));

      BlocProvider.of<WeatherBloc>(context).add(
          GetWeatherCityEvent(GetWeatherForeCastParams(city: defaultCityName)));
      Navigator.of(context).pushAndRemoveUntil(
          MaterialPageRoute(
              builder: (_) => CityWeather(
                    defaultCityName: defaultCityName,
                  )),
          (route) => false);
    } else {
      Navigator.of(context).pushAndRemoveUntil(
          MaterialPageRoute(builder: (_) => const SearchPage()),
          (route) => false);
    }
  }

  @override
  void initState() {
    getNextScreen();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
     Constants.screenWidth = MediaQuery.of(context).size.width;
    Constants.screenHeight = MediaQuery.of(context).size.height;
    return Scaffold(
      body: Stack(
        children: [
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 60),
            child: Center(child: Image.asset("assets/images/weather_logo.png")),
          ),
          Positioned(
            bottom: 20,
            left: 0,
            right: 0,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: const [
                LoadingWidget(
                  size: 50,
                ),
                SizedBox(
                  height: 15,
                ),
                Text(
                  "Weather App",
                  style: TextStyle(color: Colors.black45),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
