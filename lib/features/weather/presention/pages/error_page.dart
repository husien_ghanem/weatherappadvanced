import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:weather_app/core/resources/color_manager.dart';
import 'package:weather_app/core/routes/routes_maneger.dart';
import 'package:weather_app/features/weather/presention/bloc/weather_bloc.dart';
import '../../domain/usecases/get_weather_forecast_usecase.dart';

class ErrorPage extends StatefulWidget {
  const ErrorPage({super.key});

  @override
  State<ErrorPage> createState() => _ErrorPageState();
}

class _ErrorPageState extends State<ErrorPage> {
  String? cityName = 'tartous';

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
          centerTitle: true,
          title: const Text(
            'Some thing goes Wrong',
          ),
          backgroundColor: ColorManager.violet),
      body: Column(
        children: [
          const SizedBox(
            height: 60,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Image.asset(
                "assets/images/weather_icons/1.png",
                scale: 11,
              ),
              Image.asset(
                "assets/images/weather_icons/35.png",
                scale: 22,
              ),
            ],
          ),
          const SizedBox(
            height: 60,
          ),
          SizedBox(
            width: MediaQuery.of(context).size.width/1.11,
            child: TextField( style:  TextStyle(color: ColorManager.black),
              onSubmitted: (data) async {
                cityName = data;
              },
              onChanged: (data) {
                cityName = data;
              },
              decoration: InputDecoration(
                labelText: "Enter City Name To Get Weather",
                labelStyle: TextStyle(color: ColorManager.blue),
                enabledBorder: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(5),
                  borderSide: BorderSide(
                    color: ColorManager.blue,
                    width: 2.0,
                  ),
                ),
                focusedBorder: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(30),
                  borderSide: BorderSide(
                    color: ColorManager.violet,
                    width: 3.0,
                  ),
                ),
              ),
            ),
          ),
          const SizedBox(
            height: 30,
          ),
          ElevatedButton(
              style: ButtonStyle(
                  backgroundColor:
                      MaterialStateProperty.all(ColorManager.violet)),
              onPressed: () {
                BlocProvider.of<WeatherBloc>(context).add(GetWeatherCityEvent(
                    GetWeatherForeCastParams(city: cityName!)));
                Navigator.of(context).popAndPushNamed(Routes.cityWeatherRoute);
              },
              child: const Text(
                "get weather",
              )),
        ],
      ),
    );
  }
}
