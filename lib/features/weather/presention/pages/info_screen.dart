import 'package:flutter/material.dart';
import 'package:weather_app/core/resources/color_manager.dart';

class InfoScreen extends StatelessWidget {
  const InfoScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 0,
        leading: IconButton(
          icon: Icon(Icons.arrow_back, color: ColorManager.primary),
          onPressed: () => Navigator.of(context).pop(),
        ),
        backgroundColor: ColorManager.white,
      ),
      body: Container(
        color: ColorManager.white,
        child: Center(
          child: Container(
            decoration: BoxDecoration(
              color: ColorManager.primary,
              borderRadius: BorderRadius.circular(44),
            ),
            width: 200,
            height: 200,
            child: Container(
                decoration: BoxDecoration(
                  color: ColorManager.primary,
                  borderRadius: const BorderRadius.all(Radius.circular(44)),
                  boxShadow: [
                    BoxShadow(
                        blurRadius: 30.0,
                        color: ColorManager.primary,
                        spreadRadius: 7.0,
                        blurStyle: BlurStyle.normal
                        // shadow direction: bottom right
                        )
                  ],
                ),
                alignment: Alignment.center,
                width: 200,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text(
                      "Directed By : ",
                      style: TextStyle(
                          color: ColorManager.white,
                          fontSize: 18,
                          fontWeight: FontWeight.bold),
                    ),
                    const SizedBox(
                      height: 20,
                    ),
                    Text(
                      "Husien Ghanem ",
                      style: TextStyle(
                          color: ColorManager.white,
                          fontSize: 18,
                          fontWeight: FontWeight.bold),
                    ),
                    const Text('WhatsApp'),
                    Text(
                      "00963936219110",
                      style: TextStyle(
                          color: ColorManager.white,
                          fontSize: 18,
                          fontWeight: FontWeight.bold),
                    )
                  ],
                )),
          ),
        ),
      ),
    );
  }
}
