import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:weather_app/core/functions.dart';
import 'package:weather_app/core/resources/constans.dart';
import 'package:weather_app/features/weather/presention/widgets/image_and_data.dart';

import '../../../../core/resources/color_manager.dart';
import '../../../../core/resources/strings_manager.dart';

import '../../../../core/routes/routes_maneger.dart';
import '../../domain/entites/weather_entity.dart';
import '../bloc/weather_bloc.dart';
import '../widgets/get_main_image.dart';

class WeatherDetailes extends StatelessWidget {
  final String defaultCityName;
  const WeatherDetailes({super.key, required this.defaultCityName});

  @override
  Widget build(BuildContext context) {
    WeatherEntity? dataOfWeather;
    return BlocBuilder<WeatherBloc, WeatherState>(
      builder: (context, state) {
        if (state is LoadedWeatherState) {
          dataOfWeather = state.weatherEntity;

          return Scaffold(
              appBar: _buildAppBar(context, dataOfWeather),
              body: _buildBody(context, dataOfWeather));
        } else {
          return const SizedBox.shrink();
        }
      },
    );
  }

  Stack _buildBody(BuildContext context, WeatherEntity? dataOfWeather) {
    return Stack(
      children: [
        _buildBackRound(),
        // ListView(), //to work refresh indecator
        Column(
          children: [
            Padding(
              padding: const EdgeInsets.only(bottom: 5),
              child: Container(
                height: Constants.screenHeight / 1.6,
                width: Constants.screenWidth,
                decoration: BoxDecoration(
                  color: ColorManager.primary,
                  borderRadius: const BorderRadius.only(
                      bottomLeft: Radius.circular(70),
                      bottomRight: Radius.circular(70)),
                  boxShadow: [
                    BoxShadow(
                        blurRadius: 30.0,
                        color: ColorManager.primary,
                        spreadRadius: 7.0,
                        blurStyle: BlurStyle.normal
                        // shadow direction: bottom right
                        )
                  ],
                ),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    Stack(
                      alignment: AlignmentDirectional.bottomCenter,
                      children: [
                        _getBigImage(dataOfWeather),
                        Container(
                          alignment: Alignment.center,
                          child: Container(
                              decoration: BoxDecoration(
                                color: ColorManager.primary,
                                borderRadius:
                                    const BorderRadius.all(Radius.circular(37)),
                                boxShadow: [
                                  BoxShadow(
                                      blurRadius: 30.0,
                                      color: ColorManager.white,
                                      spreadRadius: 7.0,
                                      blurStyle: BlurStyle.normal)
                                ],
                              ),
                              alignment: Alignment.topCenter,
                              constraints:
                                  const BoxConstraints.tightFor(width: 140),
                              child: Material(
                                  color: Colors.transparent,
                                  child: Padding(
                                    padding: EdgeInsets.zero,
                                    child: degreeData(dataOfWeather, context),
                                  ))),
                        ),
                      ],
                    ),
                    Stack(
                      alignment: AlignmentDirectional.center,
                      children: [
                        Padding(
                          padding: const EdgeInsets.only(bottom: 44),
                          child: Text(
                            getDayOfWeek(
                                dataOfWeather!.forecast.forecastday[0].date),
                            style: Theme.of(context).textTheme.bodySmall,
                          ),
                        ),
                        Container(
                          height: 65,
                          alignment: Alignment.topCenter,
                          child: Text(
                              dataOfWeather.current.conditionEntity.text
                                  .toString(),
                              style: Theme.of(context).textTheme.displayLarge),
                        ),
                      ],
                    ),
                    dataOfCityRainHumidity(dataOfWeather),
                  ],
                ),
              ),
            ),
            SizedBox(
              height: 30,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.end,
                children: [
                  const Padding(
                    padding: EdgeInsets.only(left: 50),
                    child: Text('today'),
                  ),
                  Row(
                      crossAxisAlignment: CrossAxisAlignment.end,
                      children: [
                    const Text('24 hours'),
                    IconButton(
                      onPressed: () => {},
                      icon: const Icon(
                        Icons.chevron_right,
                        color: Colors.white,
                      ),
                    )
                  ]),
                ],
              ),
            ),
            SizedBox(
              height: 170,
              child: ListView.builder(
                itemExtent: 140,
                scrollDirection: Axis.horizontal,
                itemCount: dataOfWeather.forecast.forecastday[0].hour
                    .length, // specify the number of items you want in a row
                itemBuilder: (context, index) => Padding(
                  padding: const EdgeInsets.fromLTRB(0, 17, 10, 5),
                  child: Padding(
                    padding: const EdgeInsets.all(20.0),
                    child: Container(
                      height: 40,
                      width: 80,
                      decoration: BoxDecoration(
                        color: ColorManager.primary,
                        borderRadius:
                            const BorderRadius.all(Radius.circular(30)),
                        boxShadow: [
                          BoxShadow(
                              blurRadius: 30.0,
                              color: ColorManager.primary,
                              spreadRadius: 7.0,
                              blurStyle: BlurStyle.normal
                              // shadow direction: bottom right
                              )
                        ],
                      ),
                      child: Center(
                          child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Text(getHourOfday(dataOfWeather
                                  .forecast.forecastday[0].hour[index].time
                                  .toString())
                              .toString()),
                          Text(
                              "${dataOfWeather.forecast.forecastday[0].hour[index].tempC.toString()}\u00B0"),
                          Text(dataOfWeather.forecast.forecastday[0].hour[index]
                              .conditionEntity.text
                              .toString()),
                        ],
                      )),
                    ),
                  ),
                ),
              ),
            ),
          ],
        )
      ],
    );
  }

  SizedBox _getBigImage(WeatherEntity? dataOfWeather) {
    return SizedBox(
      width: Constants.screenWidth / 1.2,
      height: Constants.screenHeight / 2.4,
      child: getImagesLocalAccordingApi(
        imageString: dataOfWeather!.current.conditionEntity.icon.toString(),
      ),
    );
  }

  Container _buildBackRound() {
    return Container(
      color: ColorManager.black,
    );
  }

  Widget degreeData(WeatherEntity? dataOfWeather, BuildContext context) {
    if (dataOfWeather!.current.isDay == 1) {
      return Text(
        "${dataOfWeather.forecast.forecastday[0].day.maxtempC.toString()}\u00B0",
        style: const TextStyle(fontSize: 44),
      );
    } else if (dataOfWeather.current.isDay == 0) {
      return Stack(alignment: AlignmentDirectional.centerEnd, children: [
        Text(
          "${dataOfWeather.forecast.forecastday[0].day.mintempC.toString()}\u00B0",
          style: const TextStyle(fontSize: 44),
        ),
      ]);
    }
    return const SizedBox.shrink();
  }

  AppBar _buildAppBar(BuildContext context, WeatherEntity? dataOfWeather) {
    return AppBar(
        centerTitle: true,
        title: Padding(
          padding: const EdgeInsets.all(8.0),
          child: GestureDetector(
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                const Icon(Icons.location_on_outlined),
                Text(
                  " ${dataOfWeather!.location.name.toString()} ",
                  style: Theme.of(context).textTheme.titleLarge,
                ),
              ],
            ),
            onTap: () => Navigator.of(context).pushNamed(Routes.search),
          ),
        ),
        actions: [
          Padding(
            padding: const EdgeInsets.all(10.0),
            child: GestureDetector(
              child: Transform.scale(
                  scale: 1.4,
                  child: IconButton(
                      onPressed: () {
                        Navigator.of(context).pushNamed('InfoScreen');
                      },
                      icon: const Icon(
                        Icons.info,
                      ))),
              onTap: () {},
            ),
          ),
        ],
        leading: Padding(
          padding: const EdgeInsets.all(1.0),
          child: GestureDetector(
            child: SizedBox(
              width: 25,
              height: 25,
              child: Padding(
                padding: const EdgeInsets.all(11.0),
                child: Container(
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(20),
                        color: ColorManager.primary,
                        border:
                            Border.all(color: ColorManager.white, width: 1.4)),
                    child: const Icon(
                      Icons.search,
                      color: Colors.white,
                      size: 13,
                    )),
              ),
            ),
            onTap: () {
              Navigator.of(context).pushNamed(Routes.search);
            },
          ),
        ),
        elevation: 0,
        backgroundColor: ColorManager.primary,
        systemOverlayStyle: SystemUiOverlayStyle(
          // Status bar color
          statusBarColor: ColorManager.primary,
        )
        // systemOverlayStyle: SystemUiOverlayStyle(statusBarIconBrightness: Brightness.dark)
        );
  }

  Widget dataOfCityRainHumidity(WeatherEntity? dataOfWeather) {
    return SizedBox(
      height: 87,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: [
          getImageAndData(
              image: AppStrings.cityLogo,
              data:
                  "${dataOfWeather!.location.name.toString()}-${dataOfWeather.location.country.toString()}",
              nameOfData: dataOfWeather.location.localtime.toString(),
              scal: 12),
          getImageAndData(
              image: AppStrings.chanceOfRainLogo,
              data:
                  "${dataOfWeather.forecast.forecastday[0].day.dailyChanceOfRain}%",
              nameOfData: AppStrings.chanceOfRain,
              scal: 60),
          getImageAndData(
              image: AppStrings.humidityLogo,
              data: "${dataOfWeather.current.humidity}%",
              nameOfData: AppStrings.humidity,
              scal: 68),
        ],
      ),
    );
  }
}
