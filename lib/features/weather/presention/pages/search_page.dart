import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:weather_app/core/resources/color_manager.dart';
import 'package:weather_app/core/routes/routes_maneger.dart';
import 'package:weather_app/features/weather/presention/bloc/weather_bloc.dart';
import '../../../../core/resources/strings_manager.dart';
import '../../domain/usecases/get_weather_forecast_usecase.dart';

class SearchPage extends StatefulWidget {
  const SearchPage({super.key});

  @override
  State<SearchPage> createState() => _SearchPageState();
}

class _SearchPageState extends State<SearchPage> {
  String? cityName;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
          centerTitle: true,
          title: Text(
            'Search Page',
            style: Theme.of(context).textTheme.titleLarge,
          ),
          backgroundColor: ColorManager.violet),
      body: ListView(
        children: [
          const SizedBox(
            height: 60,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Image.asset(
                AppStrings.moonImage,
                scale: 11,
              ),
              Image.asset(
                AppStrings.sunImage,
                scale: 22,
              ),
            ],
          ),
          const SizedBox(
            height: 60,
          ),
          SizedBox(
            width: MediaQuery.of(context).size.width / 1.11,
            child: TextField(
              onSubmitted: (data) async {
                cityName = data;
              },
              onChanged: (data) {
                cityName = data;
              },
              style: TextStyle(color: ColorManager.black),
              decoration: InputDecoration(
                labelText: AppStrings.labelText,
                helperText: "in ENGLISH please",
                labelStyle: TextStyle(color: ColorManager.blue),
                enabledBorder: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(5),
                  borderSide: BorderSide(
                    color: ColorManager.blue,
                    width: 2.0,
                  ),
                ),
                focusedBorder: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(30),
                  borderSide: BorderSide(
                    color: ColorManager.violet,
                    width: 3.0,
                  ),
                ),
              ),
            ),
          ),
          const SizedBox(
            height: 30,
          ),
          ElevatedButton(
              style: ButtonStyle(
                  backgroundColor:
                      MaterialStateProperty.all(ColorManager.violet)),
              onPressed: () {
                if (cityName == null || cityName!.trim().isEmpty) {
                  Fluttertoast.showToast(msg: "Please Enter City Name");
                  return;
                }

                BlocProvider.of<WeatherBloc>(context).add(GetWeatherCityEvent(
                    GetWeatherForeCastParams(city: cityName!)));
                Navigator.of(context).popAndPushNamed(Routes.cityWeatherRoute,
                    arguments: cityName);
              },
              child: const Text(
                AppStrings.elevatedButton,
              )),
        ],
      ),
    );
  }
}
