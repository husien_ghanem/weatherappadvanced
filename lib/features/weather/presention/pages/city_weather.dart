import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:weather_app/core/functions.dart';
import 'package:weather_app/core/resources/color_manager.dart';
import 'package:weather_app/core/resources/strings_manager.dart';
import 'package:weather_app/core/routes/routes_maneger.dart';
import 'package:weather_app/features/weather/domain/entites/weather_entity.dart';
import 'package:weather_app/features/weather/presention/bloc/weather_bloc.dart';
import 'package:weather_app/features/weather/presention/pages/search_page.dart';
import '../../domain/usecases/get_weather_forecast_usecase.dart';
import '../widgets/get_main_image.dart';
import '../widgets/image_and_data.dart';
import '../widgets/show_progress_indicator.dart';
import 'error_page.dart';

class CityWeather extends StatefulWidget {
  final String defaultCityName;
  const CityWeather({super.key, required this.defaultCityName});

  @override
  State<CityWeather> createState() => _CityWeatherState();
}

class _CityWeatherState extends State<CityWeather> {
  Future<void> _refreshData() async {
    BlocProvider.of<WeatherBloc>(context).add(GetWeatherCityEvent(
        GetWeatherForeCastParams(city: widget.defaultCityName)));
    Navigator.of(context).pushAndRemoveUntil(
          MaterialPageRoute(
              builder: (_) => CityWeather(
                    defaultCityName: widget.defaultCityName,
                  )),
          (route) => false);
  }

  WeatherEntity? dataOfWeather;
  @override
  Widget build(BuildContext context) {
    return BlocConsumer<WeatherBloc, WeatherState>(
      listener: (context, state) {
        if (state is ErrorWeatherState) {
          if (state.message == "CityNotFoundFailure") {
            Fluttertoast.showToast(
                msg: "City not found!\nPlease enter another name");
            Navigator.of(context).pushAndRemoveUntil(
                MaterialPageRoute(builder: (_) => const SearchPage()),
                (route) => false);
          }
        }
      },
      builder: (context, state) {
        if (state is LoadingWeatherState) {
          return const LoadingWidget();
        } else if (state is LoadedWeatherState) {
          dataOfWeather = state.weatherEntity;
          // String saveCityName = dataOfWeather!.location.name.toString();

          return Scaffold(
              appBar: _buildAppBar(context), body: _buildBody(dataOfWeather));
        } else if (state is ErrorWeatherState) {
          return const ErrorPage();
        }
        return const SearchPage();
      },
    );
  }

  AppBar _buildAppBar(BuildContext context) {
    return AppBar(
        centerTitle: true,
        title: Padding(
          padding: const EdgeInsets.all(8.0),
          child: GestureDetector(
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                const Icon(Icons.calendar_month),
                Text(
                  " ${dataOfWeather!.forecast.forecastday.length.toString()} days",
                  style: Theme.of(context).textTheme.titleLarge,
                ),
              ],
            ),
            onTap: () => Navigator.of(context).pushNamed(Routes.search),
          ),
        ),
        actions: [
          Padding(
            padding: const EdgeInsets.all(10.0),
            child: GestureDetector(
              child: Transform.scale(
                  scale: 1.4,
                  child: IconButton(
                      onPressed: () { Navigator.of(context).pushNamed('InfoScreen');},
                      icon: const Icon(
                        Icons.info,
                      ))),
              onTap: () {
                Navigator.of(context).pushNamed('InfoScreen');
              },
            ),
          ),
        ],
        leading: Padding(
          padding: const EdgeInsets.all(1.0),
          child: GestureDetector(
            child: SizedBox(
              width: 25,
              height: 25,
              child: Padding(
                padding: const EdgeInsets.all(11.0),
                child: Container(
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(20),
                        color: ColorManager.primary,
                        border:
                            Border.all(color: ColorManager.white, width: 1.4)),
                    child: const Icon(
                      Icons.search,
                      color: Colors.white,
                      size: 13,
                    )),
              ),
            ),
            onTap: () {
              Navigator.of(context).pushNamed(Routes.search);
            },
          ),
        ),
        elevation: 0,
        backgroundColor: ColorManager.primary,
        systemOverlayStyle: SystemUiOverlayStyle(
          // Status bar color
          statusBarColor: ColorManager.primary,
        )
        // systemOverlayStyle: SystemUiOverlayStyle(statusBarIconBrightness: Brightness.dark)
        );
  }

  Widget _buildBody(WeatherEntity? dataOfWeather) {
    return RefreshIndicator(
      onRefresh: () async {
        await _refreshData();
      },
      child: Stack(
        children: [
          Container(
            color: ColorManager.black,
          ),
          ListView(), //to work refresh indecator
          topAndBottomSction(),
        ],
      ),
    );
  }

  Widget topAndBottomSction() {
    return ListView(
      children: [
        GestureDetector(
          onTap: () => Navigator.of(context).pushNamed(
              Routes.cityWeatherDetailesRoute,
              arguments: widget.defaultCityName),
          child: Container(
            height: 250,
            decoration: BoxDecoration(
              color: ColorManager.primary,
              borderRadius: const BorderRadius.only(
                  bottomLeft: Radius.circular(70),
                  bottomRight: Radius.circular(70)),
              boxShadow: [
                BoxShadow(
                    blurRadius: 30.0,
                    color: ColorManager.primary,
                    spreadRadius: 7.0,
                    blurStyle: BlurStyle.normal
                    // shadow direction: bottom right
                    )
              ],
            ),
            child: Column(
              children: [
                SizedBox(
                  height: 140, //distance from top
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    children: [
                      getImage(),
                      degreeData()!,
                    ],
                  ),
                ),
                dataOfCityRainHumidity()
              ],
            ),
          ),
        ),
        const SizedBox(
          height: 20,
        ),
        ListView.builder(
          shrinkWrap: true,
          itemCount: dataOfWeather!.forecast.forecastday.length,
          itemBuilder: (context, index) => getTheRowSmallData(index),
        )
      ],
    );
  }

  Widget getTheRowSmallData(numOfDay) {
    String dateString = dataOfWeather!.forecast.forecastday[numOfDay].date;
    return ListTile(
      contentPadding: const EdgeInsets.symmetric(horizontal: 15, vertical: 20),
      leading: SizedBox(
        width: 88,
        child: Text(
          getDayOfWeek(dateString),
          style: Theme.of(context).textTheme.displayMedium,
        ),
      ),
      title: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          SizedBox(
            height: 40,
            width: 50,
            child: getImagesLocalAccordingApi(
              imageString: dataOfWeather!
                  .forecast.forecastday[numOfDay].day.conditionEntity.icon
                  .toString(),
            ),
          ),
          const SizedBox(
            width: 10,
          ),
          Container(
              decoration: BoxDecoration(
                color: ColorManager.primary,
                borderRadius: const BorderRadius.all(Radius.circular(20)),
                boxShadow: [
                  BoxShadow(
                      blurRadius: 30.0,
                      color: ColorManager.primary,
                      spreadRadius: 7.0,
                      blurStyle: BlurStyle.normal
                      // shadow direction: bottom right
                      )
                ],
              ),
              alignment: Alignment.center,
              width: 200,
              child: Text(
                textAlign: TextAlign.center,
                dataOfWeather!
                    .forecast.forecastday[numOfDay].day.conditionEntity.text
                    .toString(),
                maxLines: 2,
                style: Theme.of(context).textTheme.displayMedium,
              ))
        ],
      ),
      trailing: SizedBox(
        height: 30,
        width: 88,
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Text(
              dataOfWeather!.forecast.forecastday[numOfDay].day.maxtempC
                  .toString(),
              style: Theme.of(context).textTheme.displayMedium,
            ),
            const Text(
              " / ",
              style: TextStyle(fontSize: 10),
            ),
            Text(
              "${dataOfWeather!.forecast.forecastday[numOfDay].day.mintempC
                  .toString()}\u00B0",
              style: Theme.of(context).textTheme.displayMedium,
            ),
          ],
        ),
      ),
    );
  }

  Widget? degreeData() {
    if (dataOfWeather!.current.isDay == 1) {
      return Column(
        mainAxisAlignment: MainAxisAlignment.center,
        // crossAxisAlignment: CrossAxisAlignment.,
        children: [
          SizedBox(
            height: 70,
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Text(
                  dataOfWeather!.forecast.forecastday[0].day.maxtempC
                      .toString(),
                  style: const TextStyle(fontSize: 50),
                ),
                Text(
                  " / ",
                  style: Theme.of(context).textTheme.bodySmall,
                ),
                Stack(alignment: AlignmentDirectional.center, children: [
                  Text(
                    "${dataOfWeather!.forecast.forecastday[0].day.mintempC
                        .toString()}\u00B0",
                    style: const TextStyle(fontSize: 20),
                  ),
                
                ]),
              ],
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(bottom: 10),
            child: Text(dataOfWeather!.current.conditionEntity.text.toString(),
                style: Theme.of(context).textTheme.bodyLarge),
          ),
        ],
      );
    } else if (dataOfWeather!.current.isDay == 0) {
      return Column(
        mainAxisAlignment: MainAxisAlignment.center,
        // crossAxisAlignment: CrossAxisAlignment.,
        children: [
          SizedBox(
            height: 70,
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Text(
                  dataOfWeather!.forecast.forecastday[0].day.maxtempC
                      .toString(),
                  style: const TextStyle(fontSize: 20),
                ),
                Text(
                  " / ",
                  style: Theme.of(context).textTheme.bodySmall,
                ),
                Stack(alignment: AlignmentDirectional.center, children: [
                  Text(
                   "${dataOfWeather!.forecast.forecastday[0].day.mintempC
                        .toString()}\u00B0",
                    style: const TextStyle(fontSize: 50),
                  ),
                 
                 
                ]),
              ],
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(bottom: 10),
            child: Text(dataOfWeather!.current.conditionEntity.text.toString(),
                style: Theme.of(context).textTheme.bodyLarge),
          ),
        ],
      );
    }
    return null;
  }

  SizedBox getImage() {
    return SizedBox(
      height: 100,
      width: 100,
      child: getImagesLocalAccordingApi(
        imageString: dataOfWeather!.current.conditionEntity.icon.toString(),
      ),
    );
  }

  Widget dataOfCityRainHumidity() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      children: [
        getImageAndData(
            image: AppStrings.cityLogo,
            data:
                "${dataOfWeather!.location.name.toString()}-${dataOfWeather!.location.country.toString()}",
            nameOfData: dataOfWeather!.location.localtime.toString(),
            scal: 12),
        getImageAndData(
            image: AppStrings.chanceOfRainLogo,
            data:
                "${dataOfWeather!.forecast.forecastday[0].day.dailyChanceOfRain}%",
            nameOfData: AppStrings.chanceOfRain,
            scal: 60),
        getImageAndData(
            image: AppStrings.humidityLogo,
            data: "${dataOfWeather!.current.humidity}%",
            nameOfData: AppStrings.humidity,
            scal: 68),
      ],
    );
  }
}
