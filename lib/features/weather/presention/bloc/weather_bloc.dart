import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:equatable/equatable.dart';

import 'package:weather_app/features/weather/domain/usecases/get_weather_forecast_usecase.dart';

import '../../../../core/functions.dart';
import '../../domain/entites/weather_entity.dart';

part 'weather_event.dart';
part 'weather_state.dart';

class WeatherBloc extends Bloc<WeatherEvent, WeatherState> {

  GetWeatherForeCastUseCase getWeatherForeCastUseCase;
  WeatherBloc(
      {required this.getWeatherForeCastUseCase})
      : super(WeatherInitial()) {
    on<WeatherEvent>((event, emit) async {
      if (event is GetWeatherCityEvent) {
        emit(LoadingWeatherState());
        final weatherOrFailure = await getWeatherForeCastUseCase(event.params);
        weatherOrFailure.fold((failure) {
          emit(ErrorWeatherState(
              message: getMessageForFailure(message: failure)));
        }, (weather) {
          emit(LoadedWeatherState(weatherEntity: weather));
        });
      } else if (event is RefreshWeatherCityEvent) {
        emit(LoadingWeatherState());
        final weatherOrFailure = await getWeatherForeCastUseCase(event.params);
        weatherOrFailure.fold((failure) {
          emit(ErrorWeatherState(
              message: getMessageForFailure(message: failure)));
        }, (weather) {
          emit(LoadedWeatherState(weatherEntity: weather));
        });
      }
    });
  }
}
