part of 'weather_bloc.dart';

abstract class WeatherEvent extends Equatable {
  const WeatherEvent();

  @override
  List<Object> get props => [];
}

class GetWeatherCityEvent extends WeatherEvent {
  final GetWeatherForeCastParams params;

const  GetWeatherCityEvent(this.params);

  @override
  List<Object> get props => [params];
}

class RefreshWeatherCityEvent extends WeatherEvent {
  final GetWeatherForeCastParams params;

  const RefreshWeatherCityEvent(this.params);
  @override
  List<Object> get props => [params];
}

