
part of 'weather_bloc.dart';

abstract class WeatherState extends Equatable {
  const WeatherState();

  @override
  List<Object> get props => [];
}

class WeatherInitial extends WeatherState {}

class LoadingWeatherState extends WeatherState {}

class LoadedWeatherState extends WeatherState {
  final WeatherEntity weatherEntity;
  const LoadedWeatherState({
    required this.weatherEntity,
  });
  @override
  List<Object> get props => [weatherEntity];
}

class ErrorWeatherState extends WeatherState {
  final String message;
  const ErrorWeatherState({
    required this.message,
  });
    @override
  List<Object> get props => [message];
}
