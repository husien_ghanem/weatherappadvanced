import 'package:equatable/equatable.dart';
import 'package:weather_app/features/weather/domain/entites/condition_entity.dart';

class CurrentEntity extends Equatable {
  final String lastUpdated;
  final double tempC;
  final int isDay;
  final ConditionEntity conditionEntity;
  final double windKph;
  final int windDegree;
  final String windDir;
  final double pressureMb;
  final double pressureIn;
  final double precipMm;
  final double precipIn;
  final int humidity;
  final int cloud;
  final double feelslikeC;
  final double visKm;
  final double gustKph;
  const CurrentEntity({
    required this.lastUpdated,
    required this.tempC,
    required this.isDay,
    required this.conditionEntity,
    required this.windKph,
    required this.windDegree,
    required this.windDir,
    required this.pressureMb,
    required this.pressureIn,
    required this.precipMm,
    required this.precipIn,
    required this.humidity,
    required this.cloud,
    required this.feelslikeC,
    required this.visKm,
    required this.gustKph,
  });

  @override
  List<Object?> get props => [
        lastUpdated,
        tempC,
        isDay,
        conditionEntity,
        windKph,
        windDegree,
        windDir,
        pressureMb,
        pressureIn,
        precipMm,
        precipIn,
        humidity,
        cloud,
        feelslikeC,
        visKm,
        gustKph,
      ];
}
