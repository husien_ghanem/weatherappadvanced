import 'package:equatable/equatable.dart';
import 'package:weather_app/features/weather/domain/entites/condition_entity.dart';

class DayEntity extends Equatable {
  const DayEntity({
    required this.maxtempC,
    required this.mintempC,
    required this.avgtempC,
    required this.maxwindKph,
    required this.totalprecipMm,
    required this.totalprecipIn,
    required this.totalsnowCm,
    required this.avghumidity,
    required this.dailyWillItRain,
    required this.dailyChanceOfRain,
    required this.dailyWillItSnow,
    required this.dailyChanceOfSnow,
    required this.conditionEntity,
  });
  final double maxtempC;

  final double mintempC;

  final double avgtempC;

  final double maxwindKph;
  final double totalprecipMm;
  final double totalprecipIn;
  final double totalsnowCm;
  final double avghumidity;
  final int dailyWillItRain;
  final int dailyChanceOfRain;
  final int dailyWillItSnow;
  final int dailyChanceOfSnow;
  final ConditionEntity conditionEntity;

  @override
  List<Object?> get props => [
        maxtempC,
        mintempC,
        avgtempC,
        maxwindKph,
        totalprecipMm,
        totalprecipIn,
        totalsnowCm,
        avghumidity,
        dailyWillItRain,
        dailyChanceOfRain,
        dailyWillItSnow,
        dailyChanceOfSnow,
        conditionEntity,
      ];
}
