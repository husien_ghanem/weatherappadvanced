
import 'package:equatable/equatable.dart';

import 'package:weather_app/features/weather/domain/entites/current_entity.dart';
import 'package:weather_app/features/weather/domain/entites/forecast_entity.dart';
import 'package:weather_app/features/weather/domain/entites/location_entity.dart';

class WeatherEntity extends Equatable {
  final LocationEntity location;
  final CurrentEntity current;
  final ForecastEntity forecast;
  const WeatherEntity({
    required this.location,
    required this.current,
    required this.forecast,
  });

  @override
  List<Object?> get props => [];
}
