import 'package:equatable/equatable.dart';

import 'package:weather_app/features/weather/domain/entites/forecastday_entity.dart';

class ForecastEntity extends Equatable {
  final List<ForecastdayEntity> forecastday;

  const ForecastEntity(this.forecastday);

  @override

  List<Object?> get props => [forecastday];
}

