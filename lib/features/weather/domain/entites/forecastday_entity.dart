import 'package:equatable/equatable.dart';
import 'package:weather_app/features/weather/domain/entites/astro_entity.dart';
import 'package:weather_app/features/weather/domain/entites/day_entity.dart';
import 'package:weather_app/features/weather/domain/entites/hour_entity.dart';

class ForecastdayEntity extends Equatable {
  const ForecastdayEntity({
    required this.date,
    required this.day,
    required this.astro,
    required this.hour,
  });
  final String date;

  final DayEntity day;
  final AstroEntity astro;
  final List<HourEntity> hour;

  @override
  List<Object?> get props => [
        date,
        day,
        astro,
        hour,
      ];
}
