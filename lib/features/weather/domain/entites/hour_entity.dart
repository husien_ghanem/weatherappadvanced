import 'package:equatable/equatable.dart';
import 'package:weather_app/features/weather/domain/entites/condition_entity.dart';

class HourEntity extends Equatable {
  final String time;
  final double? tempC;
  final double? windKph;
  final String windDir;
  final int humidity;
  final int cloud;
  final int willItRain;
  final int chanceOfRain;
  final int willItSnow;
  final int chanceOfSnow;
  final ConditionEntity conditionEntity;
  const HourEntity(
      {required this.time,
      required this.tempC,
      required this.windKph,
      required this.windDir,
      required this.humidity,
      required this.cloud,
      required this.willItRain,
      required this.chanceOfRain,
      required this.willItSnow,
      required this.chanceOfSnow,
      required this.conditionEntity});

  @override
  List<Object?> get props => [
        windDir,
        time,
        tempC,
        conditionEntity,
        windKph,
        humidity,
        cloud,
        willItRain,
        chanceOfRain,
        willItSnow,
        chanceOfSnow,
      ];
}
