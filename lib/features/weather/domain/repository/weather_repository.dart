import 'package:weather_app/core/error/error.dart';

import 'package:dartz/dartz.dart';

import '../entites/weather_entity.dart';
import '../usecases/get_weather_forecast_usecase.dart';

abstract class WeatherRebository {

  Future<Either<Failure, WeatherEntity>> getWeatherForecast(GetWeatherForeCastParams params);
}


