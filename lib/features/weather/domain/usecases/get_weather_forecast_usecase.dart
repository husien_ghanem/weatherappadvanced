import 'package:dartz/dartz.dart';
import 'package:weather_app/features/weather/domain/repository/weather_repository.dart';

import '../../../../core/error/error.dart';

import '../entites/weather_entity.dart';

class GetWeatherForeCastUseCase {


  final WeatherRebository weatherRebository;

  GetWeatherForeCastUseCase({
    required this.weatherRebository,
  });



  Future<Either<Failure, WeatherEntity>> call(
      GetWeatherForeCastParams params) async {
    
    return await weatherRebository.getWeatherForecast(params);
  }

}






class GetWeatherForeCastParams {
  
  final String city  ;
  GetWeatherForeCastParams({required this.city});
}
