import 'package:weather_app/features/weather/data/models/current_model.dart';
import 'package:weather_app/features/weather/data/models/forecast_model.dart';
import 'package:weather_app/features/weather/data/models/location_model.dart';

import '../../domain/entites/weather_entity.dart';

class WeatherModel extends WeatherEntity {
  const WeatherModel(
      {required super.location,
      required super.current,
      required super.forecast});

  factory WeatherModel.fromJson(Map<String, dynamic> json) {
    return WeatherModel(
        location: LocationModel.fromJson(json['location']),
        current: CurrentModel.fromJson(json['current']),
        forecast: ForecastModel.fromJson(json['forecast']));
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['location'] = LocationModel(
        name: location.name,
        region: location.region,
        country: location.country,
        lat: location.lat,
        lon: location.lon,
        tzId: location.tzId,
        localtimeEpoch: location.localtimeEpoch,
        localtime: location.localtime);
    data['current'] = CurrentModel(
        lastUpdated: current.lastUpdated,
        tempC: current.tempC,
        isDay: current.isDay,
        conditionEntity: current.conditionEntity,
        windKph: current.windKph,
        windDegree: current.windDegree,
        windDir: current.windDir,
        pressureMb: current.pressureMb,
        pressureIn: current.pressureIn,
        precipMm: current.precipMm,
        precipIn: current.precipIn,
        humidity: current.humidity,
        cloud: current.cloud,
        feelslikeC: current.feelslikeC,
        visKm: current.visKm,
        gustKph: current.gustKph);
    data['forecast'] = ForecastModel(forecast.forecastday);
    return data;
  }
}
