import 'package:weather_app/features/weather/data/models/condition_model.dart';

import '../../domain/entites/current_entity.dart';

class CurrentModel extends CurrentEntity {
  const CurrentModel(
      {required super.lastUpdated,
      required super.tempC,
      required super.isDay,
      required super.conditionEntity,
      required super.windKph,
      required super.windDegree,
      required super.windDir,
      required super.pressureMb,
      required super.pressureIn,
      required super.precipMm,
      required super.precipIn,
      required super.humidity,
      required super.cloud,
      required super.feelslikeC,
      required super.visKm,
      required super.gustKph});

  factory CurrentModel.fromJson(Map<String, dynamic> json) {
    return CurrentModel(
        lastUpdated: json['last_updated'],
        tempC: json['temp_c'],
        isDay: json['is_day'],
        conditionEntity: ConditionModel.fromJson(json['condition']),
        windKph: json['wind_kph'],
        windDegree: json['wind_degree'],
        windDir: json['wind_dir'],
        pressureMb: json['pressure_mb'],
        pressureIn: json['pressure_in'],
        precipMm: json['precip_mm'],
        precipIn: json['precip_in'],
        humidity: json['humidity'],
        cloud: json['cloud'],
        feelslikeC: json['feelslike_c'],
        visKm: json['vis_km'],
        gustKph: json['gust_kph']);
  }
  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['last_updated'] = lastUpdated;
    data['temp_c'] = tempC;
    data['is_day'] = isDay;
    data['condition'] = ConditionModel(
        text: conditionEntity.text,
        icon: conditionEntity.icon,
        code: conditionEntity.code);
    data['wind_kph'] = windKph;
    data['wind_degree'] = windDegree;
    data['wind_dir'] = windDir;
    data['pressure_mb'] = pressureMb;
    data['pressure_in'] = pressureIn;
    data['precip_mm'] = precipMm;
    data['precip_in'] = precipIn;
    data['humidity'] = humidity;
    data['cloud'] = cloud;
    data['feelslike_c'] = feelslikeC;
    data['vis_km'] = visKm;
    data['gust_kph'] = gustKph;
    return data;
  }
}
