import 'package:weather_app/features/weather/data/models/forecastday_model.dart';

import '../../domain/entites/forecast_entity.dart';

class ForecastModel extends ForecastEntity {
  const ForecastModel(super.forecastday);

  factory ForecastModel.fromJson(Map<String, dynamic> json) {
    return ForecastModel(List.from(json['forecastday'])
        .map((e) => ForecastdayModel.fromJson(e))
        .toList());
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['forecastday'] = forecastday
        .map((forecastdayEntity) => ForecastdayModel(
                date: forecastdayEntity.date,
                day: forecastdayEntity.day,
                astro: forecastdayEntity.astro,
                hour: forecastdayEntity.hour)
            .toJson())
        .toList();
    return data;
  }
}




// ربييييييييييييييييييييع 