import 'package:weather_app/features/weather/domain/entites/condition_entity.dart';
import 'package:weather_app/features/weather/domain/entites/hour_entity.dart';

import 'condition_model.dart';

class HourModel extends HourEntity {
  const HourModel(
      {required super.tempC,
      required super.windKph,
      required super.time,
      required super.conditionEntity,
      required super.windDir,
      required super.humidity,
      required super.cloud,
      required super.willItRain,
      required super.chanceOfRain,
      required super.willItSnow,
      required super.chanceOfSnow});

  factory HourModel.fromJson(Map<String, dynamic> json) {
    return HourModel(
        windDir: json['wind_dir'],
        time: json['time'],
        tempC: json['temp_c'],
        conditionEntity: ConditionModel.fromJson(json['condition']),
        windKph: json['wind_kph'],
        humidity: json['humidity'],
        cloud: json['cloud'],
        willItRain: json['will_it_rain'],
        chanceOfRain: json['chance_of_rain'],
        willItSnow: json['will_it_snow'],
        chanceOfSnow: json['chance_of_snow']);
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['time'] = time;
    data['temp_c'] = tempC;
    data['condition'] = fun(conditionEntity); //  is this wrong or right ?
    data['wind_kph'] = windKph;
    data['wind_dir'] = windDir;
    data['humidity'] = humidity;
    data['cloud'] = cloud;
    data['will_it_rain'] = willItRain;
    data['chance_of_rain'] = chanceOfRain;
    data['will_it_snow'] = willItSnow;
    data['chance_of_snow'] = chanceOfSnow;
    return data;
  }

  ConditionModel fun(ConditionEntity condition) {
    return ConditionModel(
        text: condition.text, icon: condition.icon, code: condition.code);
  }
}
