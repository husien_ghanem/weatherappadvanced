import 'package:weather_app/features/weather/data/models/astro_model.dart';
import 'package:weather_app/features/weather/data/models/day_model.dart';
import 'package:weather_app/features/weather/data/models/hour_model.dart';
import 'package:weather_app/features/weather/domain/entites/forecastday_entity.dart';

class ForecastdayModel extends ForecastdayEntity {
  const ForecastdayModel(
      {required super.date,
      required super.day,
      required super.astro,
      required super.hour});

  factory ForecastdayModel.fromJson(Map<String, dynamic> json) {
    return ForecastdayModel(
        date: json['date'],
        day: DayModel.fromJson(json['day']),
        astro: AstroModel.fromJson(json['astro']),
        hour:
            List.from(json['hour']).map((e) => HourModel.fromJson(e)).toList());
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['date'] = date;
    data['day'] = DayModel(
        maxtempC: day.maxtempC,
        mintempC: day.mintempC,
        avgtempC: day.avgtempC,
        maxwindKph: day.maxwindKph,
        totalprecipMm: day.totalprecipMm,
        totalprecipIn: day.totalprecipIn,
        totalsnowCm: day.totalsnowCm,
        avghumidity: day.avghumidity,
        dailyWillItRain: day.dailyWillItRain,
        dailyChanceOfRain: day.dailyChanceOfRain,
        dailyWillItSnow: day.dailyWillItSnow,
        dailyChanceOfSnow: day.dailyChanceOfSnow,
        conditionEntity: day.conditionEntity);
    data['astro'] = AstroModel(
        sunrise: astro.sunrise,
        sunset: astro.sunset,
        moonrise: astro.moonrise,
        moonset: astro.moonset,
        moonPhase: astro.moonPhase,
        moonIllumination: astro.moonIllumination);
    data['hour'] = hour
        .map((e) => HourModel(
            tempC: e.tempC,
            windKph: e.windKph,
            time: e.time,
            conditionEntity: e.conditionEntity,
            windDir: e.windDir,
            humidity: e.humidity,
            cloud: e.cloud,
            willItRain: e.willItRain,
            chanceOfRain: e.chanceOfRain,
            willItSnow: e.willItSnow,
            chanceOfSnow: e.chanceOfSnow))
        .toList();
    return data;
  }
}
