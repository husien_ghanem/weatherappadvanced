import 'package:weather_app/features/weather/data/models/condition_model.dart';

import 'package:weather_app/features/weather/domain/entites/day_entity.dart';

class DayModel extends DayEntity {
  const DayModel(
      {required super.maxtempC,
      required super.mintempC,
      required super.avgtempC,
      required super.maxwindKph,
      required super.totalprecipMm,
      required super.totalprecipIn,
      required super.totalsnowCm,
      required super.avghumidity,
      required super.dailyWillItRain,
      required super.dailyChanceOfRain,
      required super.dailyWillItSnow,
      required super.dailyChanceOfSnow,
      required super.conditionEntity});

  factory DayModel.fromJson(Map<String, dynamic> json) {
    return DayModel(
        maxtempC: json['maxtemp_c'],
        mintempC: json['mintemp_c'],
        avgtempC: json['avgtemp_c'],
        maxwindKph: json['maxwind_kph'],
        totalprecipMm: json['totalprecip_mm'],
        totalprecipIn: json['totalprecip_in'],
        totalsnowCm: json['totalsnow_cm'],
        avghumidity: json['avghumidity'],
        dailyWillItRain: json['daily_will_it_rain'],
        dailyChanceOfRain: json['daily_chance_of_rain'],
        dailyWillItSnow: json['daily_will_it_snow'],
        dailyChanceOfSnow: json['daily_chance_of_snow'],
        conditionEntity: ConditionModel.fromJson(json['condition']));
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['maxtemp_c'] = maxtempC;
    data['mintemp_c'] = mintempC;
    data['avgtemp_c'] = avgtempC;
    data['maxwind_kph'] = maxwindKph;
    data['totalprecip_mm'] = totalprecipMm;
    data['totalprecip_in'] = totalprecipIn;
    data['totalsnow_cm'] = totalsnowCm;
    data['avghumidity'] = avghumidity;
    data['daily_will_it_rain'] = dailyWillItRain;
    data['daily_chance_of_rain'] = dailyChanceOfRain;
    data['daily_will_it_snow'] = dailyWillItSnow;
    data['daily_chance_of_snow'] = dailyChanceOfSnow;
    data['condition'] = ConditionModel(
        text: conditionEntity.text,
        icon: conditionEntity.icon,
        code: conditionEntity.code);
    return data;
  }
}
