import 'package:weather_app/features/weather/domain/entites/astro_entity.dart';

class AstroModel extends AstroEntity {
  const AstroModel(
      {required super.sunrise,
      required super.sunset,
      required super.moonrise,
      required super.moonset,
      required super.moonPhase,
      required super.moonIllumination});

  factory AstroModel.fromJson(Map<String, dynamic> json) {
    return AstroModel(
        sunrise: json['sunrise'],
        sunset: json['sunset'],
        moonrise: json['moonrise'],
        moonset: json['moonset'],
        moonPhase: json['moon_phase'],
        moonIllumination: json['moon_illumination']);
  }
  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['sunrise'] = sunrise;
    data['sunset'] = sunset;
    data['moonrise'] = moonrise;
    data['moonset'] = moonset;
    data['moon_phase'] = moonPhase;
    data['moon_illumination'] = moonIllumination;
    return data;
  }
}
