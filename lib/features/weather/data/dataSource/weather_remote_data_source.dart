import 'dart:convert';

import 'package:weather_app/core/error/exception.dart';
import 'package:weather_app/features/weather/data/models/weather_model.dart';
import 'package:http/http.dart' as http;

abstract class WeatherRemoteDataSource {
  Future<WeatherModel> getWeatherForecast({
    required String cityName,
  });
}

class WeatherImplmWithHttp implements WeatherRemoteDataSource {
  final key = "3677bed892474b30b7a122242220806";
  final String baseUrlForecast = "http://api.weatherapi.com/v1/forecast";
  final numForecast = 7;

  @override
  Future<WeatherModel> getWeatherForecast({
    required String cityName,
  }) async {
    http.Response response = await http.get(Uri.parse(
        "$baseUrlForecast.json?key=$key&q=$cityName&days=5&aqi=no&alerts=no"));
    if (response.statusCode == 400) {
      throw CityNotFoundException();
    }
    if (response.statusCode == 200) {
      final data = jsonDecode(response.body);
      final weatherData = WeatherModel.fromJson(data);
      return weatherData;
    } else {
      throw ServerException();
    }
  }
}
