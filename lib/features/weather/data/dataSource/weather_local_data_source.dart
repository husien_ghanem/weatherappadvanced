
import 'dart:convert';
import 'package:dartz/dartz.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:weather_app/core/error/exception.dart';

import '../models/weather_model.dart';

abstract class WeatherLocalDataSource {
  Future<WeatherModel> getCachedWeather();
  Future<Unit> cacheWeather(WeatherModel weatherModel);
}

class WeatherLocalDataSourceImpl implements WeatherLocalDataSource {
   final SharedPreferences sharedPreferences;

  WeatherLocalDataSourceImpl({required this.sharedPreferences});
 
  @override
  Future<Unit> cacheWeather(WeatherModel weatherModel) {
    final weatherData = weatherModel.toJson();
    sharedPreferences.setString("CACHED_WEATHER", json.encode(weatherData));
    return Future.value(unit);
  }

  @override
  Future<WeatherModel> getCachedWeather() {
  final jsonString = sharedPreferences.getString("CACHED_WEATHER");

    if (jsonString != null) {
      final jsonDecoded = json.decode(jsonString);
      final weatherData = WeatherModel.fromJson(jsonDecoded);
      return Future.value(weatherData);
    } else {
      throw EmptyCacheException();
    }
}





  
  }