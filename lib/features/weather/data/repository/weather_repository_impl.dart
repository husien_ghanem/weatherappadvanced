import 'package:dartz/dartz.dart';
import 'package:weather_app/core/error/error.dart';
import 'package:weather_app/core/error/exception.dart';
import 'package:weather_app/features/weather/data/models/weather_model.dart';
import 'package:weather_app/features/weather/domain/repository/weather_repository.dart';
import '../../../../core/functions.dart';
import '../../../../core/network/network_info.dart';
import '../../domain/entites/weather_entity.dart';
import '../../domain/usecases/get_weather_forecast_usecase.dart';
import '../dataSource/weather_local_data_source.dart';
import '../dataSource/weather_remote_data_source.dart';

class WeatherRepositoryImpl implements WeatherRebository {
  final WeatherLocalDataSource weatherLocalDataSource;
  final WeatherRemoteDataSource weatherRemoteDataSource;
  NetworkInfo networkInfo;

  WeatherRepositoryImpl({
    required this.weatherLocalDataSource,
    required this.weatherRemoteDataSource,
    required this.networkInfo,
  });

  @override
  Future<Either<Failure, WeatherEntity>> getWeatherForecast(
      GetWeatherForeCastParams params) async {
    return await _getWeather(
      function: () =>
          weatherRemoteDataSource.getWeatherForecast(cityName: params.city),
    );
  }

  Future<Either<Failure, WeatherEntity>> _getWeather(
      {required Future<WeatherModel> Function() function}) async {
    if (await networkInfo.isConeccted) {
      try {
        final remoteWeather = await function();
        await weatherLocalDataSource.cacheWeather(remoteWeather);
        await setDefaultLocationSelected(remoteWeather.location.name);
        return Right(remoteWeather);
      } on ServerException {
        return Left(ServerFailure());
      } on CityNotFoundException {
        return Left(CityNotFoundFailure());
      }
    } else {
      try {
        final localWeatherData =
            await weatherLocalDataSource.getCachedWeather();
        return Right(localWeatherData);
      } on EmptyCacheException {
        return left(EmptyCacheFailure());
      }
    }
  }
}
