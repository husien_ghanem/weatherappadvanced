import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:internet_connection_checker/internet_connection_checker.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:weather_app/features/weather/data/dataSource/weather_local_data_source.dart';
import 'package:weather_app/features/weather/data/repository/weather_repository_impl.dart';
import 'package:weather_app/features/weather/domain/usecases/get_weather_forecast_usecase.dart';
import 'package:weather_app/features/weather/presention/bloc/weather_bloc.dart';
import 'core/network/network_info.dart';
import 'core/resources/theme_manager.dart';

import 'core/routes/routes_maneger.dart';
import 'features/weather/presention/pages/splash_screen.dart';
import 'features/weather/data/dataSource/weather_remote_data_source.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
  runApp(MyApp(
    sharedPreferences: sharedPreferences,
  ));
}

// ignore: must_be_immutable
class MyApp extends StatefulWidget {
  SharedPreferences sharedPreferences;
  MyApp({
    Key? key,
    required this.sharedPreferences,
  }) : super(key: key);

  @override
  State<MyApp> createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  
  @override
  Widget build(BuildContext context) {
   
    return BlocProvider(
        create: ((context) => WeatherBloc(
            getWeatherForeCastUseCase: GetWeatherForeCastUseCase(
                weatherRebository: WeatherRepositoryImpl(
                    networkInfo: NetworkInfoImpl(InternetConnectionChecker()),
                    weatherLocalDataSource: WeatherLocalDataSourceImpl(
                        sharedPreferences: widget.sharedPreferences),
                    weatherRemoteDataSource: WeatherImplmWithHttp())))),
        child: MaterialApp(
          theme: getApplicationTheme(),
          debugShowCheckedModeBanner: false,
          onGenerateRoute: RouteGenerate.generateRoute,
          home: const SplashScreen(),
        ));
  }
}
