import 'package:flutter/material.dart';
import 'package:weather_app/core/resources/styles_manager.dart';

import 'color_manager.dart';



ThemeData getApplicationTheme() {
  return ThemeData(fontFamily: 'Changa',
    // main colors
    primaryColor: ColorManager.primary,
    primaryColorLight: ColorManager.lightPrimary,
    disabledColor: ColorManager.blue,
    splashColor: ColorManager.lightPrimary,

    // app bar theme
    appBarTheme: AppBarTheme(
        centerTitle: true,
        color: ColorManager.primary,
        elevation: 4,
        shadowColor: ColorManager.lightPrimary,
        titleTextStyle:
            getRegularStyle(fontSize: 16, color: ColorManager.white)),
    // button theme
    buttonTheme: ButtonThemeData(
        shape: const StadiumBorder(),
        disabledColor: ColorManager.blue,
        buttonColor: ColorManager.primary,
        splashColor: ColorManager.lightPrimary),

    // elevated button them
    elevatedButtonTheme: ElevatedButtonThemeData(
        style: ElevatedButton.styleFrom(
            textStyle: getRegularStyle(
                color: ColorManager.white, fontSize: 17),
            // primary: ColorManager.primary,
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(12)))),

    textTheme: TextTheme(
      titleLarge: getBoldStyle(color: ColorManager.white,),
        displayLarge:
            getBoldStyle(color: ColorManager.white, fontSize: 50,),
            displayMedium:getMediumStyle(color: ColorManager.white,fontSize: 16), 
        headlineLarge:
            getSemiBoldStyle(color: ColorManager.black, fontSize: 16),
        titleMedium:
            getMediumStyle(color: ColorManager.white, fontSize: 14),
        bodyLarge: getSemiBoldStyle(color: ColorManager.white),
        bodyMedium: getMediumStyle(color: ColorManager.white),
        bodySmall: getRegularStyle(color: ColorManager.white)),
    // input decoration theme (text form field)
        inputDecorationTheme: InputDecorationTheme(
        // hint style
        hintStyle:
            getRegularStyle(color: ColorManager.grey, fontSize: 14),
        labelStyle:
            getMediumStyle(color: ColorManager.grey, fontSize: 14),
        errorStyle: getRegularStyle(color: ColorManager.error),

        // enabled border style
        enabledBorder: OutlineInputBorder(
            borderSide:
                BorderSide(color: ColorManager.primary, width: 5),
            borderRadius: const BorderRadius.all(Radius.circular(8))),

        // focused border style
        focusedBorder: OutlineInputBorder(
            borderSide:
                BorderSide(color: ColorManager.grey, width: 5),
            borderRadius: const BorderRadius.all(Radius.circular(8))),

        // error border style
        errorBorder: OutlineInputBorder(
            borderSide:
                BorderSide(color: ColorManager.error, width: 5),
            borderRadius: const BorderRadius.all(Radius.circular(8))),
        // focused border style
        focusedErrorBorder: OutlineInputBorder(
            borderSide:
                BorderSide(color: ColorManager.primary, width: 5),
            borderRadius: const BorderRadius.all(Radius.circular(8)))),
    // label style
  );  

}
