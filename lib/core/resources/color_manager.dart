import 'package:flutter/material.dart';

class ColorManager {
  static Color primary = Colors.blue;
  static Color black = Colors.black;
  static Color grey = const Color(0xff737477);
  static Color violet = const Color(0xff7147E0);

  // new colors
  static Color darkPrimary = const Color(0xffd17d11);
  static Color lightPrimary = const Color(0xCCd17d11); // color with 80% opacity
  static Color blue = const Color(0xff1280DC);
  static Color grey2 = const Color(0xff797979);
  static Color white = const Color(0xffFFFFFF);
  static Color error = const Color(0xffe61f34); // red color
}
