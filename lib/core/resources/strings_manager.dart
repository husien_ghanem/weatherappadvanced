class AppStrings {
  static const noRouteFound = "No Route Found";
  static const cityLogo = "assets/images/city_logo.png";
  static const chanceOfRainLogo = "assets/images/weather_icons/29.png";
  static const humidityLogo = "assets/images/weather_icons/4.png";
  static const humidity = "humidity";
  static const chanceOfRain = "chance of rain";
  static const tartous = "tartous";
  static const labelText = "Enter City Name To Get Weather";
  static const sunImage = "assets/images/weather_icons/35.png";
  static const moonImage = "assets/images/weather_icons/1.png";
  static const elevatedButton = "get weather";
}
