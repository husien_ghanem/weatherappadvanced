import 'package:shared_preferences/shared_preferences.dart';

import 'error/error.dart';

String getMessageForFailure({required Failure message}) {
  switch (message.runtimeType) {
    case OffLineFailure:
      return 'OffLineFailure';

    case ServerFailure:
      return 'ServerFailure';

    case EmptyCacheFailure:
      return 'OffLineFailure';

    case CityNotFoundFailure:
      return 'CityNotFoundFailure';
    default:
      return 'UnExpected Erorr';
  }
}

String getDayOfWeek(String dateString) {
  DateTime date = DateTime.parse(dateString);
  int dayOfWeek = date.weekday;

  switch (dayOfWeek) {
    case 1:
      return 'Monday';
    case 2:
      return 'Tuesday';
    case 3:
      return 'Wednesday';
    case 4:
      return 'Thursday';
    case 5:
      return 'Friday';
    case 6:
      return 'Saturday';
    case 7:
      return 'Sunday';
    default:
      return 'Invalid Day';
  }
}

String getHourOfday(String dateString) {
  DateTime date = DateTime.parse(dateString);
  int hourOfDay = date.hour;
 String formattedHour = hourOfDay.toString().padLeft(2, '0');
  String formattedTime = '$formattedHour:00';
  return formattedTime;
}

Future<bool> isDefaultLocationSelected() async {
  SharedPreferences prefs = await SharedPreferences.getInstance();
  String? seen = prefs.getString('DEFAULT_LOCATION');
  return (seen == null || seen.isEmpty) ? false : true;
}

Future<String> getDefaultLocationSelected() async {
  SharedPreferences prefs = await SharedPreferences.getInstance();
  String? defaultLocation = prefs.getString('DEFAULT_LOCATION');
  return defaultLocation!;
}

Future<void> setDefaultLocationSelected(String defaultLocation) async {
  SharedPreferences prefs = await SharedPreferences.getInstance();
  await prefs.setString('DEFAULT_LOCATION', defaultLocation);
}
