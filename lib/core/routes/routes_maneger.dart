import 'package:flutter/material.dart';
import 'package:weather_app/features/weather/presention/pages/info_screen.dart';
import '../../features/weather/presention/pages/city_weather.dart';

import '../../features/weather/presention/pages/search_page.dart';
import '../../features/weather/presention/pages/weather_detailes.dart';

class Routes {
  static const String cityWeatherRoute = "City-weather";
  static const String search = "/search";
  static const String cityWeatherDetailesRoute = "cityWeatherDetailesRoute";
    static const String infoScreen = "InfoScreen";
  
}

class RouteGenerate {
  static Route<dynamic>? generateRoute(RouteSettings settings) {
    switch (settings.name) {
      case Routes.search:
        return MaterialPageRoute(builder: (_) => const SearchPage());
         case Routes.infoScreen:
        return MaterialPageRoute(builder: (_) => const InfoScreen());
      case Routes.cityWeatherRoute:
        final String defaultCityName = settings.arguments as String;
        return MaterialPageRoute(
          builder: (_) => CityWeather(
            defaultCityName: defaultCityName,
          ),
        );
      case Routes.cityWeatherDetailesRoute:
        final String defaultCityName = settings.arguments as String;
        return MaterialPageRoute(
          builder: (_) => WeatherDetailes(
            defaultCityName: defaultCityName,
          ),
        );

      // case Routes.firstScreen:
      //   return MaterialPageRoute(builder: (_) => const FirstHome());
      // Navigator.of(context).pushNamed('LoginScreen');

      default:
        return _errorRoute();
    }
  }

  static Route<dynamic> _errorRoute() {
    return MaterialPageRoute(builder: (_) {
      return Scaffold(
        appBar: AppBar(
          title: const Text("Error"),
        ),
        body: const Center(
          child: Text("ERROR"),
        ),
      );
    });
  }
}
